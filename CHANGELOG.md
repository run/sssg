# Changelog

Changelog for SSSG. All notable changes to this project will be documented in this file.

## [0.2.2] - 2024-09-02

- License changed from MIT to GNU Affero General Public License v3.0 (AGPLv3).
- Updated `README.md` and `LICENSE` files to reflect the new license and other related changes.

## [0.2.1] - 2024-08-04

### Changed

- Enhanced error prompt in `build.rs` for improved clarity.

## [0.2.0] - 2024-07-31

### Added

- Added watch feature to monitor file changes and automatically rebuild the site.
- Introduced a new function to remove the output directory before each build in `watch.rs`.
- Added features to ignore changes on the output directory, hidden files, and temporary files using regex patterns for better performance.
- Added path normalization in `watch.rs` to handle both relative and absolute paths correctly.
- Added Ctrl-C handler to gracefully shut down the server in `watch.rs`.

### Changed

- Simplified and improved program option parsing in `main.rs` for better maintainability.
- Moved build time measurement to `build.rs` to simplify the measurement process.
- Enhanced the server startup process to ensure it serves the entire output directory correctly.

### Removed

- Removed `watch.py` file.

## [0.1.3] - 2024-07-26

### Added

- Added more error messages in `library/build.rs` to improve debugging.
- Introduced a new `load_config` function in `build.rs` for better configuration handling.
- Added more integration tests to ensure sequential execution for improved stability.

### Changed

- Improved `build.rs` by fixing function calls, removing unnecessary code, splitting functions for better modularity, and adding comments for clarity.
- Refactored `build.rs` to simplify path handling and configuration loading.
- Enhanced argument parsing, improving user input handling.
- Updated dependencies and fixed typos throughout the codebase for better maintainability.

### Removed

- Removed the 'find config.toml within directory' feature from `build.rs` to simplify the code and configuration process.

## [0.1.2] - 2024-07-20

### Added

- Added integration tests for the static site generator. These tests include sample site content for the program to use, ensuring it can successfully generate the site from this content.

### Changed

- Updated `LICENSE` file to provide updated author information.
- Updated `README.md` file with an updated program description and license information.
- Updated the program description in `Cargo.toml`.
- Fixed a typo in the `watch.py` file.

## [0.1.1] - 2023-07-12

### Changed

- Enhanced project information to provide a comprehensive overview.
- Refined the `README.md` document, focusing on clarifying the installation and build process.

### Removed

- Deprecated the `install.sh` script in favor of streamlined installation methods.

## [0.1.0] - 2022-08-17

### Added

- Introduced the `CHANGELOG.md` file to document version history.

### Changed

- The `README.md` file was updated correspondingly to reflect the addition of `CHANGELOG.md`.

## [0.0.0] - 2021-01-01

Initial Project Release

### Added

- Implemented all features grounded in the project's story and philosophy.
- Integrated an `install.sh` script to simplify the installation process.
- Added `watch.py` script, purposefully designed to enable project output rebuilding triggered by user modifications.
