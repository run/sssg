# SSSG

> sssg is a simple static site generator.

_Please visit [https://codeberg.org/run](https://codeberg.org/run) if you want to see more interesting projects._

## Story and Philosophy

There are many static site generators on the Internet. However, most of them are either too complex with extensive documentation or too simple and lacking the functionalities I want. Over the years, I spent too much time reading through documentation from others and following their rules. So, why not save time and create my own ultimate static site generator? It should be simple and powerful so that, after learning a few concepts, one could create their own static website with whatever structures, workflows, and tools they want.

Therefore, I came up with the concepts described in the following section. The implementation of SSSG makes these concepts possible. SSSG strictly follows these concepts to make the static site building experience as flexible and simple as possible, and I also created other helpful features on top of it. However, SSSG is still not in its complete stage. One big problem is that it is quite slow for a Rust program. I will keep polishing SSSG to make it better. Meanwhile, I'm excited to share SSSG with the world. I hope my work helps you generate your own static site. Code Simple, Be Powerful! :)

## Concepts and Rules

Note: Text inside `<>` are abstractions; you can replace them with whatever is reasonable.

* `<config>` - a file for all the configurations; data in the `<config>` is accessible by all the files.
* `<source>` - a directory for page files with `<page_extension>` and static files with `<source_static_extension>`.
* `<theme>` - a directory for template files with `<template_extension>` and static files with `<theme_static_extension>`.
* `<output>` - a directory for all the output files, including all the static files and files with `<output_extension>`.
* `<page_extension>` - extension for pages; it could be in one or more formats (e.g. `.md`, `.html`).
* `<template_extension>` - extension for templates; it could be in one or more formats (e.g. `.hbs`, `.tera`).
* `<source_static_extension>` - extension for static files in `<source>`; it could be in one or more formats (e.g. `.html`, `.png`, `.ico`). All static files will be copied to the `<output>` with the same relative path.
* `<theme_static_extension>` - extension for static files in `<theme>`; it could be in one or more formats (e.g. `.html`, `.png`, `.ico`). All static files will be copied to the `<output>` with the same relative path.
* `<output_extension>` - extension for output files when combining page with template; only one format is allowed (normally it will be `.html`).
* All page files ending with `<page_extension>` in the `<source>` will be combined with files with the same relative path ending with `<template_extension>` in `<theme>`. The result will be saved to `<output>` with the same relative path as a directory, and the `index` plus `<output_extension>` as the file name.

## Installation 

You can install the `sssg` binary directly from the remote git repo:

```bash
cargo install --git https://codeberg.org/run/sssg.git
```

Or you can install it from a local source after cloning the project:

```bash
cd sssg
cargo install --path .
```

## Build

You can also build the project manually using:

```bash
cargo build
```

By default, the binary path is `<sssg>/target/debug/sssg` (replace `<sssg>` with the location of the `sssg` directory on your machine).

You can add the path in your `~/.bashrc`:

```bash
export PATH=$PATH:<sssg>/target/debug/
```

## Usage

You can always use `-h` or `--help` to check how commands work.

### Build

To build the site, simply go to your site directory and enter:

```bash
sssg build
```

Here are the steps SSSG will follow when building your site:

1. Read the `<config>` and load all the settings.
2. Copy all files ending with `<theme_static_extension>` in the `<theme>` to the `<output>` with the same relative path.
3. Generate all the list pages in `<config>`.
4. Copy all files ending with `<source_static_extension>` in the `<source>` to the `<output>` with the same relative path.
5. Combine all page files ending with `<page_extension>` in the `<source>` with the same relative path ending with `<template_extension>` in `<theme>`. The result will be saved to `<output>` with the same relative path as a directory, and the `index` plus `<output_extension>` as the file name.

### Watch

To watch for file changes and automatically rebuild the site, simply go to your site directory and enter:

```bash
sssg watch
```

The watch command will monitor file changes in the source and theme directories, automatically rebuilding the site as needed.

### Configuration

The configuration file is `<config>` (by default, it is `config.toml`). All the data inside `<config>` will be available to all the files.

You can use the `config` variable to access all the data in `<config>`.

Here are some variables with special meanings:

* `path_config` - A list of configurations for a path.
    - `path` - This is a necessary variable inside the `path_config`. It tells SSSG what path to configure.
    - `page_template` - Specify a template for all pages in this path.
    - `list_template` - Specify a template to create a list of all files in this path.
    - `list_size` - Set the size of a list. SSSG will automatically split all items into multiple lists when the number of items exceeds the list size.
    - `list_url_format` - Set the URL format for the list page (you can use `$page_num` as a placeholder for the page number).
    - `list_output` - Output directory for a list.

### List

A list is a special type of page. It will only appear when you manually set it up in the `<config>`. It contains a list of pages with some special variables below:

* `list` - Access all data available in the list.
    - `page` - This is exactly the same as `page` in the Page section.
    - `total` - Total page number in the list.
* `pagination` - Pagination
    - `current_page` - Current page.
    - `prev_page` - Previous page.
    - `next_page` - Next page.

### Page

A page normally appears in two parts: front matter and content. It contains the following special variables:

* `page` - Access all data available in the page.
    - `path` - Path for the current page.
    - `content` - Page content.

#### Front Matter

Data inside the front matter is local to the file.

SSSG supports the following formats, each with its own identifying tokens:

* TOML - Start and end with `+++`.
* YAML - Start and end with `---`.

#### Content

You can access content with the `content` variable.

You can add a `<!-- more -->` keyword inside the page file. It helps you automatically split page content into two parts: `less` and `more`.

#### Parser

SSSG will automatically choose the parser based on the file extension.

It is possible to use whatever page parser you want. However, SSSG only supports the following page parsers so far:

* [pulldown-cmark](https://github.com/raphlinus/pulldown-cmark)

### Render

#### Include

`_include` is a special directory; all the valid files inside it can be visible to other template files at or below the same level as `_include`. The file inside `_include` can be accessed by its relative path (e.g., a file inside `_include/base.tera` can be represented using `base.tera` or `base`).

#### Template Engine

SSSG will automatically choose the template engine based on the file extension.

It is possible to use whatever template engine you want. However, SSSG only supports the following template languages so far:

* [handlebars-rust](https://github.com/sunng87/handlebars-rust)
* [tera](https://tera.netlify.app/)

##### Handlebars-rust

Files with the extension `.hbs` will be rendered by handlebars-rust automatically.

Check the handlebars language syntax [here](https://handlebarsjs.com/), and please check the [handlebars-rust repo](https://github.com/sunng87/handlebars-rust) for more advanced features.

Here are some helper functions SSSG provides:

* `markdown` - This function helps convert markdown to HTML.
* `plaintext` - This function helps convert HTML to plain text.
* `date` - This function converts any `datetime` format to a specified one. It takes two parameters: the first parameter is the `datetime` string, and the second parameter is the format. Check out more details about the format [here](https://docs.rs/chrono/*/chrono/format/strftime/index.html).

##### Tera

Files with the extension `.tera` will be rendered by Tera automatically. Please check the Tera language syntax [here](https://tera.netlify.app/docs/).

Here are some helper functions SSSG provides:

* `markdown` - This function helps convert markdown to HTML. It has the input parameter `content`.
* `plaintext` - This function helps convert HTML to plain text. It has the input parameter `content`.
* `date` - This function converts any `datetime` format to a specified one. It takes two parameters: parameter `data` is the `datetime` string, and parameter `format` is the format. Check out more details about the format [here](https://docs.rs/chrono/*/chrono/format/strftime/index.html).

## Changelog

Check [CHANGELOG.md](CHANGELOG.md) for more details.

## License

[![](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)](https://www.gnu.org/licenses/agpl-3.0.html)

This project is licensed under the GNU Affero General Public License v3.0 (AGPLv3). Check [LICENSE](LICENSE) for more details.

Copyright (c) 2020-2024 run <https://codeberg.org/run>. All rights reserved.
