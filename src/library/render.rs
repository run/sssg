use std::fs;
use std::path::{Path, PathBuf};
use std::collections::HashMap;

use toml;
use handlebars::*;
use tera::Tera;
use walkdir::{DirEntry, WalkDir};
use dtparse;
use regex;

use super::page;

pub fn template(path: &Path, context: toml::map::Map<String, toml::Value>) -> Result<String, Box<dyn std::error::Error>>{
    match path.extension().unwrap().to_str().unwrap() {
        "hbs" => hbs_template(path, context),
        "tera" => tera_template(path, context),
        _ => Err("Unsupport template format!")?,
    }
}

fn hbs_template(path: &Path, context: toml::map::Map<String, toml::Value>) -> Result<String, Box<dyn std::error::Error>>{

    let hbs_extension = ".hbs";

    // search all templates under current dir
    let template_paths = file_by_extension(path.parent().unwrap(), hbs_extension);
    let mut include_path = path.components();
    let include_path = include_path.next();
    let include_path = Path::new(&include_path.unwrap()).join(Path::new("_include"));
    let include_paths = file_by_extension(&include_path, hbs_extension);
    
    // register template with given file name 
    let mut reg = handlebars::Handlebars::new();
    reg.set_strict_mode(true);

    for template_path in include_paths {
        let template_content = fs::read_to_string(&template_path)
            .expect("Something went wrong reading the file");

        let template_register_name = &template_path.file_stem().unwrap().to_string_lossy();
        if let Err(e) = reg.register_template_string(template_register_name, template_content) { println!("Err: can't register `{}` ({})", template_register_name, e) };
    }

    for template_path in template_paths {
        let template_content = fs::read_to_string(&template_path)
            .expect("Something went wrong reading the file");

        let template_register_name = &template_path.file_stem().unwrap().to_string_lossy();
        if let Err(e) = reg.register_template_string(template_register_name, template_content) { println!("Err: can't register `{}` ({})", template_register_name, e) };
    }
    
    // helpers
    /// this function helps convert markdown to html
    fn markdown_hbs_helper(
        h: &handlebars::Helper,
        _: &handlebars::Handlebars,
        _: &handlebars::Context,
        _: &mut handlebars::RenderContext,
        out: &mut dyn handlebars::Output,
    ) -> handlebars::HelperResult {
        let p = h.param(0).expect("Please use a valid parameter for markdown");
        let content = p.value().render();
        out.write(&page::markdown2html(&content)).expect("Can't write markdown");
        Ok(())
    }
    /// this function helps convert html to plain text
    fn plaintext_hbs_helper(
        h: &handlebars::Helper,
        _: &handlebars::Handlebars,
        _: &handlebars::Context,
        _: &mut handlebars::RenderContext,
        out: &mut dyn handlebars::Output,
    ) -> handlebars::HelperResult {
        let re = regex::Regex::new(r"<(.*?)>|\n").unwrap();
        let p = h.param(0).expect("Please use a valid parameter for markdown");
        let content = p.value().render();
        out.write(&re.replace_all(&content, "").to_owned()).expect("Can't write markdown");
        Ok(())
    }
    /// this function converts any datetime formats to a specified one
    /// it takes two parameters:
    /// first parameter is the datetime string, second parameter is the format.
    fn date_hbs_helper(
        h: &handlebars::Helper,
        _: &handlebars::Handlebars,
        _: &handlebars::Context,
        _: &mut handlebars::RenderContext,
        out: &mut dyn handlebars::Output,
    ) -> handlebars::HelperResult {
        let s = h.param(0).expect("Please use a valid string for datetime");
        let s = s.value().render();
        // more details: https://docs.rs/chrono/*/chrono/format/strftime/index.html
        let fmt = h.param(1).expect("Please use a valid user format for datetime");
        let fmt = fmt.value().render();
        let (custom, _) = dtparse::parse(&s).expect("Couldn't parse date from given string");
        let date = &custom.format(&fmt).to_string();
        out.write(date).expect("Can't write date");
        Ok(())
    }
    reg.register_helper("markdown", Box::new(markdown_hbs_helper));
    reg.register_helper("plaintext", Box::new(plaintext_hbs_helper));
    reg.register_helper("date", Box::new(date_hbs_helper));
      
    // render and save 
    let render = reg.render(&path.file_stem().unwrap().to_string_lossy(), &context)?;

    Ok(render)
}

fn tera_template(path: &Path, context: toml::map::Map<String, toml::Value>) -> Result<String, Box<dyn std::error::Error>>{
    let tera_extension = ".tera";

    let mut include_path = path.components();
    let include_path = include_path.next();
    let include_path = Path::new(&include_path.unwrap()).join(Path::new("_include"));

    let tera_dir = format!("{}/**/*{}", include_path.display(), tera_extension);
    let mut tera = Tera::parse(&tera_dir).expect("Something went wrong reading the file");

    let mut register_vector: Vec<(PathBuf, Option<String>)> = Vec::new();
    let include_paths = file_by_extension(&include_path, tera_extension);
    let template_paths = file_by_extension(path.parent().unwrap(), tera_extension);

    for template_path in include_paths {
        let template_register_name = &template_path.file_stem().unwrap().to_string_lossy();
        let template_register_name = template_register_name.to_string();
        register_vector.push((template_path, Some(template_register_name)));
    }

    for template_path in template_paths {
        let template_register_name = &template_path.file_stem().unwrap().to_string_lossy();
        let template_register_name = template_register_name.to_string();
        register_vector.push((template_path, Some(template_register_name)));
    }

    /// this function helps convert markdown to html
    /// it has the input parameter `content`
    fn markdown_tera_helper(args: &HashMap<String, tera::Value>) -> tera::Result<tera::Value> {
        match args.get("content") {
            Some(c) => match tera::from_value::<String>(c.clone()) {
                Ok(v) => Ok(tera::Value::String(page::markdown2html(&v).to_owned())),
                Err(_) => Err("Please use a valid parameter for markdown".into()),
            },
            _ => Err("Can't write markdown".into()),
        }
    }
    /// this function helps convert html to plain text
    /// it has the input parameter `content`
    fn plaintext_tera_helper(args: &HashMap<String, tera::Value>) -> tera::Result<tera::Value> {
        let re = regex::Regex::new(r"<(.*?)>|\n").unwrap();
        match args.get("content") {
            Some(c) => match tera::from_value::<String>(c.clone()) {
                Ok(v) => Ok(tera::Value::String(re.replace_all(&v, "").to_owned().to_string())),
                Err(_) => Err("Please use a valid parameter for markdown".into()),
            },
            _ => Err("Can't write markdown".into()),
        }
    }
    /// this function converts any datetime formats to a specified one
    /// it takes two parameters:
    /// parameter `data` is the datetime string, parameter `format` is the format.
    fn date_tera_helper(args: &HashMap<String, tera::Value>) -> tera::Result<tera::Value> {
        let date = args.get("date").expect("Please use a valid string for datetime");
        let date = date.as_str().expect("Please use a valid string for datetime");
        // more details: https://docs.rs/chrono/*/chrono/format/strftime/index.html
        let fmt = args.get("format").expect("Please use a valid user format for datetime");
        let fmt = fmt.as_str().expect("Please use a valid user format for datetime");
        let (custom, _) = dtparse::parse(date).expect("Couldn't parse date from given string");
        let date = &custom.format(fmt).to_string();
        Ok(tera::Value::String(date.to_string()).to_owned())
    }
    tera.register_function("markdown", markdown_tera_helper);
    tera.register_function("plaintext", plaintext_tera_helper);
    tera.register_function("date", date_tera_helper);

    tera.add_template_files(register_vector).expect("Something went wrong adding the template file");
    tera.build_inheritance_chains()?;

    let render = tera.render(&path.file_stem().unwrap().to_string_lossy(), &tera::Context::from_serialize(&context).unwrap())?;
    Ok(render)
}

fn file_by_extension(path: &Path, extension: &str) -> Vec<PathBuf> {
    let mut subpaths = Vec::new();

    fn is_hidden(entry: &DirEntry) -> bool {
        entry.file_name()
            .to_str()
            .map(|s| s.starts_with(".") || s.starts_with("_"))
            .unwrap_or(false)
    }

    for subpath in WalkDir::new(path)
        .into_iter()
        .filter_entry(|e| !is_hidden(e) || e.path() == path)
        .filter_map(|e| e.ok()) {
            let f_name = subpath.file_name().to_string_lossy();
            if f_name.ends_with(&extension) {
                subpaths.push(subpath.path().to_path_buf());
            }
        }

    subpaths
}
