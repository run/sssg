use std::io::BufReader;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;

use toml;
use pulldown_cmark;

struct Page {
    frontmatter: String,
    content: String,
    less: String,
    more: String,
}

pub enum Markdown {
    On, // all pages will be parsed as Markdown
    Off, // all pages will NOT be parsed as Markdown
    Auto, // only page with .md will be parsed as Markdown
}

pub fn get_markdown_enum(option: &str) -> Option<Markdown> {
    match option.to_lowercase().as_str() {
        "on" => Some(Markdown::On),
        "off" => Some(Markdown::Off),
        "auto" => Some(Markdown::Auto),
        _ => None,
    }
}

pub struct Options<'a> {
    pub path: &'a Path,
    pub markdown: &'a Markdown,
}

/// parse given path with options
pub fn parse(page_path: &Path, options: Options) -> toml::map::Map<String, toml::Value> {
    let parse_markdown = match options.markdown {
        Markdown::On => true,
        Markdown::Off => false,
        Markdown::Auto => page_path.extension().unwrap() == "md",
    };
    let (page, frontmatter_format) = split_page(page_path);

    let mut map: toml::map::Map<String, toml::Value> = match frontmatter_format.as_str() {
        "yaml" => serde_yaml::from_str(&(page.frontmatter)).unwrap(),
        _ => toml::from_str(&(page.frontmatter)).unwrap(),
    };

    //map.insert("frontmatter".to_string(), toml::Value::String(page.frontmatter));
    if parse_markdown {
        map.insert("content".to_string(), toml::Value::String(markdown2html(&page.content)));
        map.insert("less".to_string(), toml::Value::String(markdown2html(&page.less)));
        map.insert("more".to_string(), toml::Value::String(markdown2html(&page.more)));
    } else {
        map.insert("content".to_string(), toml::Value::String(page.content));
        map.insert("less".to_string(), toml::Value::String(page.less));
        map.insert("more".to_string(), toml::Value::String(page.more));
    }
    map.insert("path".to_string(), toml::Value::String(options.path.to_string_lossy().to_string()));

    map
}

/// convert markdown to html
pub fn markdown2html(markdown_input: &str) -> String {
    let options = pulldown_cmark::Options::all();
    let parser = pulldown_cmark::Parser::new_ext(markdown_input, options);

    let parser = parser.map(|event| match event {
        pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link(link_type, link, title)) => {
            if (link_type == pulldown_cmark::LinkType::Inline || link_type == pulldown_cmark::LinkType::Autolink) && (link.starts_with("http:") || link.starts_with("https:") || link.starts_with("//")) {
                return pulldown_cmark::Event::Html(format!("<a href=\"{}\" target=\"_blank\" rel=\"noopener noreferrer nofollow\">", link).to_owned().into())
            }
            pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link(link_type, link, title))    
        },
        _ => event
    });

    let mut html_output = String::new();
    pulldown_cmark::html::push_html(&mut html_output, parser);

    html_output
}

/// check if line contains `<!-- more -->`
fn with_more(line: &str) -> bool {
    line.contains("<!-- more -->") || line.contains("<!--more -->") || line.contains("<!-- more-->") || line.contains("<!--more-->")
}

fn split_page(page_path: &Path) -> (Page, String) {
    const TOML_FRONTMATTER_IDENTIFIER: &str = "+++";
    const YAML_FRONTMATTER_IDENTIFIER: &str = "---";

    let frontmatter_start = [TOML_FRONTMATTER_IDENTIFIER, YAML_FRONTMATTER_IDENTIFIER];
    let mut frontmatter_end = String::new();
    let mut frontmatter_format = String::new();

    let file = File::open(page_path).unwrap();

    let mut frontmatter = String::new();
    let mut less = String::new();
    let mut more = String::new();

    let reader = BufReader::new(file);
    let mut is_frontmatter = false;
    let mut is_content = false;
    let mut is_more = false;
    for line in reader.lines() {
        let line = line.unwrap();

        if !is_content {
            if is_frontmatter && line == frontmatter_end {
                is_frontmatter = false;
                is_content = true;
                continue
            } else if frontmatter_start.contains(&line.as_str()) {
                match line.as_str() {
                    TOML_FRONTMATTER_IDENTIFIER => {
                        frontmatter_format = "toml".to_string();
                        frontmatter_end = TOML_FRONTMATTER_IDENTIFIER.to_string();
                    },
                    YAML_FRONTMATTER_IDENTIFIER => {
                        frontmatter_format = "yaml".to_string();
                        frontmatter_end = YAML_FRONTMATTER_IDENTIFIER.to_string();
                    },
                    _ => (),
                };
                is_frontmatter = true;
                continue
            }
        }

        if is_frontmatter {
            frontmatter.push_str(&line);
            frontmatter.push('\n');
        } else if is_content {
            if is_more {
                more.push_str(&line);
                more.push('\n');
            } else {
                if with_more(&line) {
                    is_more = true;
                    continue
                }
                less.push_str(&line);
                less.push('\n');
            }
        }
    }

    frontmatter.pop();
    less.pop();
    more.pop();

    let content: String = format!("{}{}", less, more);

    if !is_more {
        less = String::new();
        more = String::new();
    }

    let page = Page {
        frontmatter,
        content,
        less,
        more
    };

    (page, frontmatter_format)
}
