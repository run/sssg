use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use std::path::{Path, PathBuf};
use std::fs;

use actix_files;
use actix_web::{rt, App, HttpServer};
use notify::{RecommendedWatcher, RecursiveMode, Watcher, Config};
use regex::Regex;
use ctrlc;

use super::build;

/// Watches for file system changes and rebuilds the project accordingly.
/// Also starts a web server to serve the built site.
pub fn watch(build_options: &build::Options, address: &str, port: u16) {

    // Shared flag to signal the application should exit
    let should_exit = Arc::new(Mutex::new(false));
    let should_exit_clone = Arc::clone(&should_exit);

    // Set up Ctrl+C handler to update the should_exit flag
    ctrlc::set_handler(move || {
        println!("Ctrl+C received, shutting down...");
        let mut should_exit = should_exit_clone.lock().unwrap();
        *should_exit = true;
    }).expect("Error setting Ctrl-C handler");

    // Initial build process
    println!("Initial build...");
    clean_output_directory(build_options.output.unwrap_or("_site"));
    build::new(build_options);

    // Convert address to string
    let address = address.to_string();

    // Create a channel to receive file system events
    let (tx, rx) = channel();
    let mut watcher = RecommendedWatcher::new(tx, Config::default()).unwrap();

    // Get the output path and current directory
    let output_path = build_options.output.unwrap_or("_site").to_string();
    let current_dir = std::env::current_dir().unwrap();
    let full_output_path = normalize_path(&current_dir, &output_path);

    // Watch the current directory for changes
    watcher.watch(&current_dir, RecursiveMode::Recursive).unwrap();

    // Start the server in a new thread
    let output_path_clone = full_output_path.clone();
    let server_handle = thread::spawn(move || {
        let server_future = HttpServer::new(move || {
            App::new()
                .service(actix_files::Files::new("/", &*output_path_clone).index_file("index.html"))
        })
        .bind((address.clone(), port))
        .unwrap()
        .run();

        println!("Serving HTTP on {} port {} (http://{}:{}/)", address, port, address, port);

        // Run the Actix server
        rt::System::new().block_on(server_future).unwrap();
    });

    // Get the list of patterns to ignore
    let ignore_patterns = get_ignore_patterns();

    // Main loop to handle file system events
    while !*should_exit.lock().unwrap() {
        if let Ok(Ok(event)) = rx.recv_timeout(Duration::from_millis(400)) {
            // Check if the event should be ignored
            if event.paths.iter().any(|path| should_ignore(path.strip_prefix(&current_dir).unwrap_or(path), &ignore_patterns)) {
                continue;
            }

            // Handle the change event
            println!("Change detected: {:?}", event.paths);
            clean_output_directory(build_options.output.unwrap_or("_site"));
            build::new(build_options);
            println!("Rebuilt site.");
        }
    }

    // Wait for the server thread to finish
    println!("Stopping server...");
    server_handle.join().unwrap();

    println!("Have a good day! :)");
}

/// Normalizes a given path to ensure it's absolute.
fn normalize_path(current_dir: &Path, path: &str) -> PathBuf {
    let path = Path::new(path);
    if path.is_absolute() {
        path.to_path_buf()
    } else {
        current_dir.join(path)
    }
}

/// Cleans the output directory by removing it if it exists.
fn clean_output_directory(output_dir: &str) {
    let path = Path::new(output_dir);
    if path.exists() {
        if let Err(e) = fs::remove_dir_all(path) {
            println!("Failed to remove output directory: {:?}", e);
        }
    }
}

/// Returns a list of regex patterns to ignore certain files and directories.
fn get_ignore_patterns() -> Vec<Regex> {
    vec![
        Regex::new(r"^\..+").unwrap(),    // Hidden files and directories (start with .)
        Regex::new(r"(^|/)_").unwrap(),   // Hidden files and directories (start with _)
        Regex::new(r"4913$").unwrap(),    // Specific temporary file created by Vim
        Regex::new(r"\.tmp$").unwrap(),   // Temporary files with .tmp extension
        Regex::new(r"\.sw[po]$").unwrap(),// Vim swap files
        Regex::new(r"~$").unwrap(),       // Backup files ending with ~
        Regex::new(r"\.DS_Store$").unwrap(), // macOS .DS_Store files
        Regex::new(r"\.bak$").unwrap(),   // Backup files ending with .bak
    ]
}

/// Checks if the given path should be ignored based on the provided patterns.
fn should_ignore(path: &Path, patterns: &[Regex]) -> bool {
    let path_str = path.to_str().unwrap_or("");
    patterns.iter().any(|pattern| pattern.is_match(path_str))
}
