use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::time::Instant;

use dtparse;
use toml;
use walkdir::{DirEntry, WalkDir};

use super::page;
use super::render;

pub struct Options<'a> {
    pub config: Option<&'a str>,
    pub source: Option<&'a str>,
    pub theme: Option<&'a str>,
    pub output: Option<&'a str>,
    pub page_extension: Vec<&'a str>,
    pub template_extension: Vec<&'a str>,
    pub source_static_extension: Vec<&'a str>,
    pub theme_static_extension: Vec<&'a str>,
    pub output_extension: Option<&'a str>,
    pub parse_options: ParseOptions,
}

pub struct ParseOptions {
    pub markdown: Option<page::Markdown>,
}

/// Main function to start the build process and measure the build time
pub fn new(options: &Options) {
    let start = Instant::now();
    if let Err(e) = build(options) {
        eprintln!("Build failed: {}", e);
    } else {
        let duration = start.elapsed();
        println!("Build time: {:?}", duration);
    }
}

/// Start the build process
fn build(options: &Options) -> Result<(), String> {
    println!("> Build");

    // Paths setup
    let config_path = Path::new(options.config.unwrap_or("config.toml"));
    let source_path = Path::new(options.source.unwrap_or("source"));
    let theme_path = Path::new(options.theme.unwrap_or("theme"));
    let output_path = Path::new(options.output.unwrap_or("_site"));

    // Load the configuration
    let config_map = load_config(config_path)?;
    let path_config_map = get_path_config(&config_map)?;

    // Parsing options
    let parse_markdown = options.parse_options.markdown.as_ref().unwrap_or(&page::Markdown::Auto);
    let output_extension = options.output_extension.unwrap_or("html");

    // Copy static files from theme directory to output directory
    copy_static_files(theme_path, output_path, &options.theme_static_extension)?;

    let mut touched_page_list: Vec<PathBuf> = Vec::new();

    // Process list files as per path configuration
    for (path, config) in &path_config_map {
        if let Some(list_config) = &config.list {
            let list_path = source_path.join(path);
            let list_template = get_template_path(theme_path, &list_config.list_template, &options.template_extension)?;
            let page_path = get_page_path(source_path, &list_template, &options.page_extension);
            touched_page_list.push(page_path.clone());

            let list_output = output_path.join(list_config.list_output.as_ref().unwrap());
            let options = ListRenderOptions {
                config_path,
                list_path: &list_path,
                page_path: &page_path,
                template_path: &list_template,
                list_size: list_config.list_size.unwrap(),
                list_url_format: list_config.list_url_format.as_ref().unwrap(),
                output_path: &list_output,
                output_extension,
                parse_markdown,
            };
            list_render(options)?;
        }
    }

    // Copy and render files from source directory to output directory
    copy_and_render_files(
        source_path,
        output_path,
        &path_config_map,
        &touched_page_list,
        &options.page_extension,
        &options.template_extension,
        &options.source_static_extension,
        theme_path,
        config_path,
        output_extension,
        parse_markdown,
    )?;

    Ok(())
}

/// Load and parse the configuration file
fn load_config(config_path: &Path) -> Result<toml::map::Map<String, toml::Value>, String> {
    let config_file = fs::read_to_string(config_path)
        .map_err(|e| format!("Error: Unable to read config file `{}`: {}", config_path.display(), e))?;
    toml::from_str(&config_file)
        .map_err(|e| format!("Error: Unable to parse config file `{}`: {}", config_path.display(), e))
}

/// Copy static files from theme directory to output directory
fn copy_static_files(theme_path: &Path, output_path: &Path, static_extensions: &[&str]) -> Result<(), String> {
    for selected_file in WalkDir::new(theme_path)
        .into_iter()
        .filter_entry(|e| !is_hidden_file(e))
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
    {
        let selected_file = selected_file.path();
        if let Some(ext) = selected_file.extension().and_then(|e| e.to_str()) {
            if static_extensions.contains(&ext) {
                let target_loc = output_path.join(path_without_root(selected_file));
                copy_file(selected_file, &target_loc)?;
            }
        }
    }
    Ok(())
}

/// Get the path of the template file
fn get_template_path(theme_path: &Path, template: &Option<String>, extensions: &[&str]) -> Result<PathBuf, String> {
    let template_path = theme_path.join(Path::new(template.as_ref().unwrap()));
    get_path_by_extension(&template_path, extensions).ok_or_else(|| {
        format!(
            "Error: Unable to find template `{}` with extensions {:?}",
            template_path.display(),
            extensions
        )
    })
}

/// Get the path of the page file
fn get_page_path(source_path: &Path, template_path: &Path, extensions: &[&str]) -> PathBuf {
    let page_path = source_path.join(path_without_root(template_path)).with_extension("");
    get_path_by_extension(&page_path, extensions).unwrap_or(page_path)
}

/// Copy and render files from source directory to output directory
fn copy_and_render_files(
    source_path: &Path,
    output_path: &Path,
    path_config_map: &HashMap<PathBuf, PathConfig>,
    touched_page_list: &[PathBuf],
    page_extensions: &[&str],
    template_extensions: &[&str],
    static_extensions: &[&str],
    theme_path: &Path,
    config_path: &Path,
    output_extension: &str,
    parse_markdown: &page::Markdown,
) -> Result<(), String> {
    for selected_file in WalkDir::new(source_path)
        .into_iter()
        .filter_entry(|e| !is_hidden_file(e))
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
    {
        let selected_file = selected_file.path();
        if let Some(ext) = selected_file.extension().and_then(|e| e.to_str()) {
            if page_extensions.contains(&ext) && !touched_page_list.contains(&selected_file.to_path_buf()) {
                let selected_file_noroot = path_without_root(selected_file).with_extension("");
                let template_path = theme_path.join(&selected_file_noroot);

                if let Some(template_path) = get_path_by_extension(&template_path, template_extensions) {
                    let output = output_path.join(&selected_file_noroot);
                    let options = PageRenderOptions {
                        config_path,
                        page_path: selected_file,
                        template_path: &template_path,
                        output_path: &output,
                        output_extension,
                        parse_markdown,
                    };
                    page_render(options)?;
                } else if let Some(path_config) = path_config_map.get(&selected_file_noroot.parent().unwrap().to_path_buf()) {
                    if let Some(page_config) = &path_config.page {
                        let page_template = theme_path.join(Path::new(&page_config.page_template.as_ref().unwrap()));
                        if let Some(page_template) = get_path_by_extension(&page_template, template_extensions) {
                            let output = output_path.join(&selected_file_noroot);
                            let options = PageRenderOptions {
                                config_path,
                                page_path: selected_file,
                                template_path: &page_template,
                                output_path: &output,
                                output_extension,
                                parse_markdown,
                            };
                            page_render(options)?;
                        }
                    }
                }
            } else if static_extensions.contains(&ext) {
                let target_loc = output_path.join(path_without_root(selected_file));
                copy_file(selected_file, &target_loc)?;
            }
        }
    }
    Ok(())
}

/// Remove the root component of a path
fn path_without_root(path: &Path) -> PathBuf {
    path.iter().skip(1).collect()
}

/// Get path with the correct extension
fn get_path_by_extension(path: &Path, extensions: &[&str]) -> Option<PathBuf> {
    if path.extension().is_none() {
        for ext in extensions {
            let p = path.with_extension(ext);
            if p.exists() {
                return Some(p);
            }
        }
    } else if path.exists() {
        return Some(path.to_path_buf());
    }
    None
}

/// Create a new file and write content to it
fn new_file(path: &Path, content: &str) -> Result<(), String> {
    let path_parent = path.parent().unwrap();
    if let Err(e) = fs::create_dir_all(path_parent) {
        return Err(format!("Error: Unable to create directory `{}`: {}", path_parent.display(), e));
    }

    if let Err(e) = fs::write(path, content) {
        return Err(format!("Error: Unable to create file `{}`: {}", path.display(), e));
    }

    Ok(())
}

/// Copy a file to a new location
fn copy_file(source: &Path, destination: &Path) -> Result<(), String> {
    if !destination.exists() {
        if let Err(e) = fs::create_dir_all(destination.parent().unwrap()) {
            return Err(format!(
                "Error: Unable to create directory `{}`: {}",
                destination.parent().unwrap().display(),
                e
            ));
        }
    }

    if let Err(e) = fs::copy(source, destination) {
        return Err(format!(
            "Error: Unable to copy file from `{}` to `{}`: {}",
            source.display(),
            destination.display(),
            e
        ));
    }

    println!("Info: `{}` => `{}`", source.display(), destination.display());
    Ok(())
}

/// Check if a file is hidden
fn is_hidden_file(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with('.') || s.starts_with('_'))
        .unwrap_or(false)
}

// Struct to hold page render options
struct PageRenderOptions<'a> {
    config_path: &'a Path,
    page_path: &'a Path,
    template_path: &'a Path,
    output_path: &'a Path,
    output_extension: &'a str,
    parse_markdown: &'a page::Markdown,
}

/// Render a page
fn page_render(options: PageRenderOptions) -> Result<(), String> {
    let output_path = if options.output_path.file_stem().unwrap() == "index" {
        options.output_path.with_extension(options.output_extension)
    } else {
        options.output_path.join("index").with_extension(options.output_extension)
    };

    let config_file = fs::read_to_string(options.config_path)
        .map_err(|e| format!("Error: Unable to read config file `{}`: {}", options.config_path.display(), e))?;
    let config: toml::map::Map<String, toml::Value> = toml::from_str(&config_file)
        .map_err(|e| format!("Error: Unable to parse config file `{}`: {}", options.config_path.display(), e))?;

    let option_path = Path::new("/").join(path_without_root(&output_path)).parent().unwrap().to_path_buf();
    let page_options = page::Options {
        path: &option_path,
        markdown: options.parse_markdown,
    };
    let page = page::parse(options.page_path, page_options);

    let mut context = toml::map::Map::new();
    context.insert("page".to_string(), toml::Value::Table(page));
    context.insert("config".to_string(), toml::Value::Table(config));

    match render::template(options.template_path, context) {
        Ok(result) => {
            new_file(&output_path, &result)?;
            println!("Info: `{}` + `{}` => `{}`", options.page_path.display(), options.template_path.display(), output_path.display());
            Ok(())
        }
        Err(e) => Err(format!(
            "Error: Unable to render page `{}` + `{}` => `{}`: {}",
            options.page_path.display(),
            options.template_path.display(),
            output_path.display(),
            e
        )),
    }
}

// Struct to hold list render options
struct ListRenderOptions<'a> {
    config_path: &'a Path,
    list_path: &'a Path,
    page_path: &'a Path,
    template_path: &'a Path,
    list_size: i64,
    list_url_format: &'a str,
    output_path: &'a Path,
    output_extension: &'a str,
    parse_markdown: &'a page::Markdown,
}

/// Render a list of pages
fn list_render(options: ListRenderOptions) -> Result<(), String> {
    let config_file = fs::read_to_string(options.config_path)
        .map_err(|e| format!("Error: Unable to read config file `{}`: {}", options.config_path.display(), e))?;
    let config: toml::map::Map<String, toml::Value> = toml::from_str(&config_file)
        .map_err(|e| format!("Error: Unable to parse config file `{}`: {}", options.config_path.display(), e))?;

    let mut list_vector: Vec<toml::Value> = Vec::new();

    for selected_file in WalkDir::new(options.list_path)
        .min_depth(1)
        .max_depth(1)
        .into_iter()
        .filter_entry(|e| !is_hidden_file(e))
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
    {
        let selected_file = selected_file.path();
        let option_path = Path::new("/").join(path_without_root(selected_file)).with_extension("");
        let page_options = page::Options {
            path: &option_path,
            markdown: options.parse_markdown,
        };
        let page = page::parse(selected_file, page_options);

        list_vector.push(toml::Value::Table(page));
    }

    list_vector.sort_by(|a, b| {
        let t = toml::value::Value::String("1970-01-01".to_string());
        let s = "1970-01-01";
        let a = a.as_table().unwrap().get("date").unwrap_or(&t);
        let b = b.as_table().unwrap().get("date").unwrap_or(&t);
        let (a, _) = dtparse::parse(a.as_str().unwrap_or(s)).unwrap();
        let (b, _) = dtparse::parse(b.as_str().unwrap_or(s)).unwrap();
        b.cmp(&a)
    });

    let mut current_page = 1;
    let mut current_vector: Vec<toml::Value> = Vec::new();
    let mut current_list_num = 0;

    for e in &list_vector {
        current_vector.push(e.clone());
        current_list_num += 1;

        if current_vector.len() as i64 == options.list_size || current_list_num == list_vector.len() {
            let mut page_context = toml::map::Map::new();
            let mut pagination_context = toml::map::Map::new();
            let mut context = toml::map::Map::new();

            let pagination_page = options.output_path.to_path_buf();
            let mut pagination_page = pagination_page.components();
            let out_dir = pagination_page.next().unwrap();
            let pagination_page = Path::new(&pagination_page).to_path_buf();

            let index_with_extension = Path::new("index").with_extension(options.output_extension);

            let mut substitute_map = HashMap::new();
            substitute_map.insert("page_num".to_string(), current_page.to_string());
            let current_page_name = substitute_path(options.list_url_format, substitute_map);
            let pagination_page_current = match current_page {
                1 => pagination_page.to_str().unwrap().to_string(),
                _ => pagination_page
                    .join(Path::new(&current_page_name))
                    .to_str()
                    .unwrap()
                    .to_string(),
            };
            let output = Path::new(&out_dir)
                .join(Path::new(&pagination_page_current))
                .join(&index_with_extension);
            let pagination_page_current = format!("/{}", pagination_page_current);

            let pagination_page_prev = match current_page {
                1 => String::from(""),
                2 => {
                    let pagination_page_prev = pagination_page.to_str().unwrap().to_string();
                    format!("/{}", pagination_page_prev)
                }
                _ => {
                    let mut substitute_map = HashMap::new();
                    substitute_map.insert("page_num".to_string(), (current_page - 1).to_string());
                    let prev_page_name = substitute_path(options.list_url_format, substitute_map);
                    let pagination_page_prev = pagination_page
                        .join(Path::new(&prev_page_name))
                        .to_str()
                        .unwrap()
                        .to_string();
                    format!("/{}", pagination_page_prev)
                }
            };

            let pagination_page_next = if current_list_num == list_vector.len() {
                String::from("")
            } else {
                let mut substitute_map = HashMap::new();
                substitute_map.insert("page_num".to_string(), (current_page + 1).to_string());
                let next_page_name = substitute_path(options.list_url_format, substitute_map);
                let pagination_page_next = pagination_page
                    .join(Path::new(&next_page_name))
                    .to_str()
                    .unwrap()
                    .to_string();
                format!("/{}", pagination_page_next)
            };

            page_context.insert("page".to_string(), toml::Value::Array(current_vector.clone()));
            page_context.insert("total".to_string(), toml::Value::Integer(list_vector.len() as i64));
            pagination_context.insert(
                "current_page".to_string(),
                toml::Value::String(pagination_page_current),
            );
            pagination_context.insert(
                "prev_page".to_string(),
                toml::Value::String(pagination_page_prev),
            );
            pagination_context.insert(
                "next_page".to_string(),
                toml::Value::String(pagination_page_next),
            );
            context.insert("list".to_string(), toml::Value::Table(page_context));
            context.insert(
                "pagination".to_string(),
                toml::Value::Table(pagination_context),
            );
            context.insert("config".to_string(), toml::Value::Table(config.clone()));

            if options.page_path.exists() {
                let option_path = Path::new("/").join(path_without_root(&output)).parent().unwrap().to_path_buf();
                let page_options = page::Options {
                    path: &option_path,
                    markdown: options.parse_markdown,
                };
                let page = page::parse(options.page_path, page_options);
                context.insert("page".to_string(), toml::Value::Table(page));
            }

            match render::template(options.template_path, context) {
                Ok(result) => {
                    new_file(&output, &result)?;
                    println!("Info: `{}` + `{}` => `{}`", options.list_path.display(), options.template_path.display(), output.display());
                },
                Err(e) => {
                    return Err(format!("Error: Unable to render list `{}` + `{}` => `{}`: {}", options.list_path.display(), options.template_path.display(), output.display(), e));
                }
            };

            current_vector.clear();
            current_page += 1;
        }
    }
    Ok(())
}

/// Substitute placeholders in the path with actual values
fn substitute_path(path: &str, substitute_map: HashMap<String, String>) -> String {
    let mut path = path.to_string();
    for (key, value) in substitute_map {
        let key = format!("${}", key);
        path = path.replace(&key, &value);
    }
    path
}

// Struct to hold page configuration
struct PageConfig {
    page_template: Option<String>,
}

// Struct to hold list configuration
struct ListConfig {
    list_template: Option<String>,
    list_size: Option<i64>,
    list_url_format: Option<String>,
    list_output: Option<String>,
}

// Struct to hold path configuration
struct PathConfig {
    page: Option<PageConfig>,
    list: Option<ListConfig>,
}

/// Get path configuration from the configuration map
fn get_path_config(config: &toml::map::Map<String, toml::Value>) -> Result<HashMap<PathBuf, PathConfig>, String> {
    let mut path_config = HashMap::new();

    let path_config_array = match config.get("path_config") {
        Some(array) => match array.as_array() {
            Some(array) => array,
            _ => return Ok(path_config),
        },
        _ => return Ok(path_config),
    };

    for p in path_config_array {
        if let Some(p) = p.as_table() {
            if let Some(path) = p.get("path").and_then(|v| v.as_str()) {
                let path = Path::new(path).to_path_buf();

                let page_config = p.get("page_template").and_then(|v| v.as_str()).map(|page_template| PageConfig {
                    page_template: Some(page_template.to_string()),
                });

                let list_config = if let (Some(list_template), Some(list_size), Some(list_url_format), Some(list_output)) = (
                    p.get("list_template").and_then(|v| v.as_str()),
                    p.get("list_size").and_then(|v| v.as_integer()),
                    p.get("list_url_format").and_then(|v| v.as_str()),
                    p.get("list_output").and_then(|v| v.as_str()),
                ) {
                    Some(ListConfig {
                        list_template: Some(list_template.to_string()),
                        list_size: Some(list_size),
                        list_url_format: Some(list_url_format.to_string()),
                        list_output: Some(list_output.to_string()),
                    })
                } else {
                    None
                };

                let p_config = PathConfig { page: page_config, list: list_config };

                path_config.insert(path, p_config);
            }
        }
    }

    Ok(path_config)
}
