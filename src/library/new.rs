use std::fs;
use std::path::Path;
use serde::Serialize;
use std::time::SystemTime;
use chrono::offset::Local;
use chrono::DateTime;


pub struct Options<'a> {
    pub path: Option<&'a str>,
    pub format: Option<&'a str>,
    pub title: Option<&'a str>,
    pub author: Option<&'a str>,
}

pub fn page(options: Options) {

    let path = Path::new(options.path.unwrap());
    let format = match options.format {
        Some(format) => format.to_lowercase(),
        _ => "toml".to_string(),
    };
    let format = format.as_str();
    let title = match options.title {
        Some(title) => title,
        _ => path.file_stem().unwrap().to_str().unwrap(),
    };
    let author = options.author.unwrap_or_default();

    if path.exists() {
        println!("Error: `{}` already exists", path.display());
        std::process::exit(1);
    };

    if let Err(e) = fs::create_dir_all(path.parent().unwrap()) {
        println!("Error: unable to create `{}` ({:?})", path.display(), e);
        std::process::exit(1);
    }

    #[derive(Serialize)]
    struct FrontMatter<'a>{
        title: &'a str,
        date: &'a str,
        author: &'a str,
    }

    let system_time = SystemTime::now();
    let datetime: DateTime<Local> = system_time.into();
    //let date = datetime.format("%Y-%m-%d %T");
    let date = &datetime.format("%Y-%m-%d").to_string();

    let front_matter = FrontMatter {
        title,
        date,
        author,
    };

    let (top, mid, bottom) = match format {
        "yaml" | "yml" => {
            let top = "";
            let bottom = "\n---\n";
            (top, serde_yaml::to_string(&front_matter).unwrap(), bottom)
        },
        "toml" => {
            let top = "+++\n";
            let bottom = "+++\n";
            (top, toml::to_string(&front_matter).unwrap(), bottom)
        },
        _ => {
            println!("Error: Please enter a valid format");
            std::process::exit(1);
        },
    };

    let page_content = format!("{}{}{}", top, mid, bottom);

    if let Err(e) = fs::write(path, page_content) {
        println!("Error: unable to create `{}` ({:?})", path.display(), e);
        std::process::exit(1);
    }

    println!("new page: {}", path.display());
}
