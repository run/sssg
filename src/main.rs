#[macro_use]
extern crate clap;
use clap::{Arg, Command, ArgMatches};

mod library;
use library::build;
use library::new;
use library::page;
use library::watch;

fn main() {
    let matches = Command::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(
            Command::new("new")
                .about("create new page for your site")
                .arg(Arg::new("path").help("page path").required(true))
                .arg(
                    Arg::new("format")
                        .help("front matter format (toml/yaml)")
                        .short('f')
                        .long("format")
                        .default_value("toml"),
                )
                .arg(Arg::new("title").help("page title").short('p').long("page"))
                .arg(
                    Arg::new("author")
                        .help("author name")
                        .short('a')
                        .long("author")
                        .default_value("")
                        .hide_default_value(true),
                ),
        )
        .subcommand(
            Command::new("build")
                .about("build your site")
                .arg(
                    Arg::new("config")
                        .help("config file directory")
                        .short('c')
                        .long("config")
                        .default_value("config.toml")
                        .display_order(1),
                )
                .arg(
                    Arg::new("source")
                        .help("source directory")
                        .short('s')
                        .long("source")
                        .default_value("source")
                        .display_order(1),
                )
                .arg(
                    Arg::new("theme")
                        .help("theme directory")
                        .short('t')
                        .long("theme")
                        .default_value("theme")
                        .display_order(1),
                )
                .arg(
                    Arg::new("output")
                        .help("output directory")
                        .short('o')
                        .long("output")
                        .default_value("_site")
                        .display_order(1),
                )
                .arg(
                    Arg::new("markdown")
                        .help("choose how markdown parser works (on/off/auto)")
                        .long("markdown")
                        .default_value("auto")
                        .display_order(2),
                )
                .arg(
                    Arg::new("page-extension")
                        .help("set page's file extension(s)")
                        .long("page-extension")
                        .default_value("md")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("template-extension")
                        .help("set template's file extension(s)")
                        .long("template-extension")
                        .default_value("hbs,tera")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("source-static-extension")
                        .help("set static file extension(s) in source directory")
                        .long("source-static-extension")
                        .default_value("js,css,png,jpg,html")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("theme-static-extension")
                        .help("set static file extension(s) in theme directory")
                        .long("theme-static-extension")
                        .default_value("js,css,png,jpg")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("output-extension")
                        .help("set output file extension")
                        .long("output-extension")
                        .default_value("html")
                        .display_order(3),
                ),
        )
        .subcommand(
            Command::new("watch")
                .about("watch for changes and rebuild your site")
                .arg(
                    Arg::new("config")
                        .help("config file directory")
                        .short('c')
                        .long("config")
                        .default_value("config.toml")
                        .display_order(1),
                )
                .arg(
                    Arg::new("source")
                        .help("source directory")
                        .short('s')
                        .long("source")
                        .default_value("source")
                        .display_order(1),
                )
                .arg(
                    Arg::new("theme")
                        .help("theme directory")
                        .short('t')
                        .long("theme")
                        .default_value("theme")
                        .display_order(1),
                )
                .arg(
                    Arg::new("output")
                        .help("output directory")
                        .short('o')
                        .long("output")
                        .default_value("_site")
                        .display_order(1),
                )
                .arg(
                    Arg::new("markdown")
                        .help("choose how markdown parser works (on/off/auto)")
                        .long("markdown")
                        .default_value("auto")
                        .display_order(2),
                )
                .arg(
                    Arg::new("page-extension")
                        .help("set page's file extension(s)")
                        .long("page-extension")
                        .default_value("md")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("template-extension")
                        .help("set template's file extension(s)")
                        .long("template-extension")
                        .default_value("hbs,tera")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("source-static-extension")
                        .help("set static file extension(s) in source directory")
                        .long("source-static-extension")
                        .default_value("js,css,png,jpg,html")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("theme-static-extension")
                        .help("set static file extension(s) in theme directory")
                        .long("theme-static-extension")
                        .default_value("js,css,png,jpg")
                        .display_order(3)
                        .value_delimiter(','),
                )
                .arg(
                    Arg::new("output-extension")
                        .help("set output file extension")
                        .long("output-extension")
                        .default_value("html")
                        .display_order(3),
                )
                .arg(
                    Arg::new("address")
                        .help("address to serve the site")
                        .short('a')
                        .long("address")
                        .default_value("127.0.0.1")
                        .display_order(4),
                )
                .arg(
                    Arg::new("port")
                        .help("port to serve the site")
                        .short('p')
                        .long("port")
                        .default_value("8000")
                        .display_order(4),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("new", m)) => {
            let new_options = new::Options {
                path: m.get_one::<String>("path").map(|s| s.as_str()),
                format: m.get_one::<String>("format").map(|s| s.as_str()),
                title: m.get_one::<String>("title").map(|s| s.as_str()),
                author: m.get_one::<String>("author").map(|s| s.as_str()),
            };

            new::page(new_options);
        }
        Some(("build", m)) => {
            let build_options = parse_build_options(m);
            build::new(&build_options);
        }
        Some(("watch", m)) => {
            let build_options = parse_build_options(m);
            let port = m.get_one::<String>("port").unwrap_or(&"8000".to_string()).parse::<u16>().unwrap_or(8000);
            let address = m.get_one::<String>("address").map(|s| s.as_str()).unwrap_or("127.0.0.1");
            watch::watch(&build_options, address, port);
        }
        _ => println!(
            "{} {}\nFor more information try --help",
            crate_name!(),
            crate_version!()
        ),
    }
}

fn parse_build_options(matches: &ArgMatches) -> build::Options {
    build::Options {
        config: matches.get_one::<String>("config").map(|s| s.as_str()),
        source: matches.get_one::<String>("source").map(|s| s.as_str()),
        theme: matches.get_one::<String>("theme").map(|s| s.as_str()),
        output: matches.get_one::<String>("output").map(|s| s.as_str()),
        page_extension: matches.get_many::<String>("page-extension").unwrap().map(String::as_str).collect(),
        template_extension: matches.get_many::<String>("template-extension").unwrap().map(String::as_str).collect(),
        source_static_extension: matches.get_many::<String>("source-static-extension").unwrap().map(String::as_str).collect(),
        theme_static_extension: matches.get_many::<String>("theme-static-extension").unwrap().map(String::as_str).collect(),
        output_extension: matches.get_one::<String>("output-extension").map(|s| s.as_str()),
        parse_options: build::ParseOptions {
            markdown: page::get_markdown_enum(matches.get_one::<String>("markdown").unwrap()),
        },
    }
}
