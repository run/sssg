use std::env;
use std::fs;
use std::path::Path;
use std::process::Command;
use std::sync::{Mutex, Once};

static INIT: Once = Once::new();
static TEST_MUTEX: Mutex<()> = Mutex::new(());

#[test]
fn test_generate_site_binary() {
    let _guard = TEST_MUTEX.lock().unwrap();
    run_test(
        "tests/sample_site",
        "tests/sample_site/_site",
        "tests/sample_site_expect",
        &[]
    );
}

#[test]
fn test_generate_site_with_full_options() {
    let _guard = TEST_MUTEX.lock().unwrap();
    run_test(
        "tests/sample_site_with_full_options",
        "tests/sample_site_with_full_options/_custom_output",
        "tests/sample_site_with_full_options_expect",
        &[
            "--config", "custom_config.toml",
            "--source", "custom_source",
            "--theme", "custom_theme",
            "--output", "_custom_output",
            "--markdown", "on",
            "--page-extension", "md,html",
            "--template-extension", "hbs,tera",
            "--source-static-extension", "js,css,png,jpg,html",
            "--theme-static-extension", "js,css,png,jpg",
            "--output-extension", "html"
        ]
    );
}

#[test]
fn test_generate_site_with_partial_options() {
    let _guard = TEST_MUTEX.lock().unwrap();
    run_test(
        "tests/sample_site_with_partial_options",
        "tests/sample_site_with_partial_options/_custom_output",
        "tests/sample_site_with_partial_options_expect",
        &[
            "--config", "custom_config.toml",
            "--source", "custom_source",
            "--theme", "custom_theme",
            "--output", "_custom_output",
            "--markdown", "on",
            "--page-extension", "md,html",
            "--template-extension", "hbs,tera",
            "--source-static-extension", "css,png,jpg,html",
            "--theme-static-extension", "js,css,png,jpg",
            "--output-extension", "html"
        ]
    );
}

fn run_test(input_dir: &str, output_dir: &str, expected_output_dir: &str, options: &[&str]) {
    // Ensure global initialization
    INIT.call_once(|| {
        // Perform any one-time initialization here, if needed
    });

    // Get the current working directory (project root)
    let project_root = env::current_dir().unwrap();

    // Construct paths relative to the project root
    let input_dir = project_root.join(input_dir);
    let output_dir = project_root.join(output_dir);
    let expected_output_dir = project_root.join(expected_output_dir);

    // Clean up the output directory before the test
    if output_dir.exists() {
        fs::remove_dir_all(&output_dir).unwrap();
    }

    // Change the current directory to the input directory
    env::set_current_dir(&input_dir).unwrap();

    // Ensure the binary is built before running the tests
    let build_output = Command::new("cargo")
        .arg("build")
        .output()
        .expect("Failed to build the project");

    assert!(build_output.status.success());

    // Run the binary with options
    let mut args = vec!["run", "--", "build"];
    args.extend_from_slice(options);

    let output = Command::new("cargo")
        .args(&args)
        .output()
        .expect("Failed to execute command");

    // Log the output for debugging
    if !output.status.success() {
        eprintln!("STDOUT: {}", String::from_utf8_lossy(&output.stdout));
        eprintln!("STDERR: {}", String::from_utf8_lossy(&output.stderr));
    }

    // Check if the command was successful
    assert!(output.status.success());

    // Restore the project root directory
    env::set_current_dir(&project_root).unwrap();

    // Compare the generated content with the expected content
    compare_directories(&expected_output_dir, &output_dir);
}

fn compare_directories(expected: &Path, generated: &Path) {
    for entry in fs::read_dir(expected).unwrap() {
        let entry = entry.unwrap();
        let expected_entry_path = entry.path();
        let generated_entry_path = generated.join(entry.file_name());

        if expected_entry_path.is_dir() {
            compare_directories(&expected_entry_path, &generated_entry_path);
        } else {
            compare_files(&expected_entry_path, &generated_entry_path);
        }
    }
}

fn compare_files(expected: &Path, generated: &Path) {
    let expected_content = fs::read(expected).unwrap_or_else(|err| {
        panic!(
            "Failed to read expected file {}: {}",
            expected.display(),
            err
        );
    });

    let generated_content = fs::read(generated).unwrap_or_else(|err| {
        panic!(
            "Failed to read generated file {}: {}",
            generated.display(),
            err
        );
    });

    assert_eq!(
        expected_content,
        generated_content,
        "File content does not match for {:?}",
        generated.file_name()
    );
}
